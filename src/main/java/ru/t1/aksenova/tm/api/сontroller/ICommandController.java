package ru.t1.aksenova.tm.api.сontroller;

public interface ICommandController {

    void showWelcome();

    void showInfo();

    void showArgumentError();

    void showCommandError();

    void showAbout();

    void showVersion();

    void showCommands();

    void showArguments();

    void showHelp();

}
