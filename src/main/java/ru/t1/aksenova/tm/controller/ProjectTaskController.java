package ru.t1.aksenova.tm.controller;

import ru.t1.aksenova.tm.api.service.IProjectTaskService;
import ru.t1.aksenova.tm.api.сontroller.IProjectTaskController;
import ru.t1.aksenova.tm.util.TerminalUtil;

public class ProjectTaskController implements IProjectTaskController {

    private final IProjectTaskService projectTaskService;

    public ProjectTaskController(final IProjectTaskService projectTaskService) {
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void bindTaskToProject() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();

        System.out.println("ENTER TASK ID:");
        final String taskId = TerminalUtil.nextLine();

        projectTaskService.bindTaskToProject(projectId, taskId);
    }

    @Override
    public void unbindTaskToProject() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();

        System.out.println("ENTER TASK ID:");
        final String taskId = TerminalUtil.nextLine();

        projectTaskService.unbindTaskToProject(projectId, taskId);
    }

}

